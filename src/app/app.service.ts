import { Injectable } from '@angular/core';


import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

// Importing the required files for http services.
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import * as ROSLIB from 'roslib';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public dynamicWayPoint;
  public ros;
  public id = 'aFtqWvud';
  public url = 'wss://dev.flytbase.com/websocket';
  public url1 = 'https://dev.flytbase.com';
  public namespace = 'flytsim';
  public msg;
  public gpsData: any;

  public token: any = 'bc33dc1a850fd56e733f361e2283a6bd4668512b';

  public headerDict = {
    'Authorization': `Token ${this.token}`,
    'VehicleID': 'aFtqWvud'
  };

  public takeOff_height = { takeoff_alt: 5 };

  public wp = {
    'waypoints': [
      {
        'frame': 3,
        'command': 16,
        'is_current': false,
        'autocontinue': true,
        'param1': 0,
        'param2': 0,
        'param3': 0,
        'param4': 0,
        'x_lat': 37.4302557845417,
        'y_long': -122.08297020393326,
        'z_alt': 5
      }, {
        'frame': 3,
        'command': 16,
        'is_current': false,
        'autocontinue': true,
        'param1': 0,
        'param2': 0,
        'param3': 0,
        'param4': 0,
        'x_lat': 37.43033672166647,
        'y_long': -122.08206361728622,
        'z_alt': 5
      }, {
        'frame': 3,
        'command': 16,
        'is_current': false,
        'autocontinue': true,
        'param1': 0,
        'param2': 0,
        'param3': 0,
        'param4': 0,
        'x_lat': 37.4298468377311,
        'y_long': -122.08258933025314,
        'z_alt': 5
      }]
  };



  constructor(public http: HttpClient) {
    this.ros = new ROSLIB.Ros({
      url: this.url
    });

    const authService = new ROSLIB.Service({
      ros: this.ros,
      name: '/websocket_auth'
    });

    const request = new ROSLIB.ServiceRequest({
      vehicleid: 'aFtqWvud',
      authorization: `Token ${this.token}`
    });

    this.ros.on('connection', () => {
      console.log('connected to web socket');

      authService.callService(request, result => {
        if (result.success) {
          console.log(result.message);
          this.gpsData = new ROSLIB.Topic({
            ros: this.ros,
            name: `/${this.namespace}/mavros/global_position/global`,
            messageType: 'sensor_msgs/NavSatFix',
            throttle_rate: 2000
          });

          this.gpsData.subscribe(function (message) {
            this.msg = message;
          });
        }
      });


    });
    this.ros.on('error', error => {
      console.log('Error connecting to web socket server: ', error);
    });

    this.ros.on('close', () => {
      console.log('Connection to web socket server closed.');
    });

  }

  public takeOff(): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url1}/rest/ros/${this.namespace}/navigation/take_off`, this.takeOff_height, { headers: this.headerDict });
  }

  public land(): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url1}/rest/ros/${this.namespace}/navigation/land`, { async: false }, { headers: this.headerDict });
  }

  public setWayPoint(): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url1}/rest/ros/${this.namespace}/navigation/waypoint_set`, this.wp, { headers: this.headerDict });
  }

  // setting mission for drone from map
  public setWayPointDynamically(waypoints): Observable<any> {

    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url1}/rest/ros/${this.namespace}/navigation/waypoint_set`, { 'waypoints': waypoints }, { headers: this.headerDict });

  }

  // getting mission from drone on map
  public getMission(): Observable<any> {
    return this.http.get(`${this.url1}/rest/ros/${this.namespace}/navigation/waypoint_get`, { headers: this.headerDict });
  }

  // clearing mission for drone from map
  public clearMission(): Observable<any> {
    return this.http.get(`${this.url1}/rest/ros/${this.namespace}/navigation/waypoint_clear`, { headers: this.headerDict });
  }

  // start mission for drone from map OR execute mission
  public executeMission(): Observable<any> {
    return this.http.get(`${this.url1}/rest/ros/${this.namespace}/navigation/waypoint_execute`, { headers: this.headerDict });
  }

  // pause mission for drone from map OR pause mission
  public pauseMission(): Observable<any> {
    return this.http.get(`${this.url1}/rest/ros/${this.namespace}/navigation/waypoint_pause`, { headers: this.headerDict });
  }
}


