import { Component, OnInit, ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { AppService } from './../app.service';

// for toastr messages
import { ToastrService } from 'ngx-toastr';
import { addListener } from 'cluster';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  polygon: google.maps.Polygon;
  place: any;
  public places: any;
  public path = [];
  public markers = {};
  public flightPath: any;
  public msg;
  public dronemarker;

  public wayPoints = [];
  public waypointmarker;

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  // @ViewChild('plc') gmapElmt: any;
  @ViewChild('search')
  public searchElementRef: any;


  constructor(public appService: AppService, private toastr: ToastrService) { }

  ngOnInit() {

    // for place searches
    this.places = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);

    // for setting initial position
    const mapProp = {
      center: new google.maps.LatLng(18.531093, 73.856671),
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      disableDoubleClickZoom: true
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    google.maps.event.addListener(this.map, 'click', (event) => {
      this.placeMarker(event.latLng);
    });


    const intervl = setInterval(() => {
      // console.log(this.appService.gpsData);
      if (this.appService.gpsData) {
        console.log('got data');
        this.appService.gpsData.subscribe(message => {
          const loc = new google.maps.LatLng(message.latitude, message.longitude);
          this.map.panTo(loc);
          if (this.dronemarker) {
            this.dronemarker.setPosition(loc);
          } else {
            this.dronemarker = new google.maps.Marker({
              position: loc,
              map: this.map,
              title: '#' + this.path.length,
              draggable: true,
            });

          }
          // console.log('Altitude : ' + message.altitude);
          // console.log('Longitude : ' + message.longitude);
          // console.log('Latitude : ' + message.latitude);
        });
        clearInterval(intervl);
      }
    }, 1000);


    this.getWaypoints();
  }

  placeMarker(location) {
    this.path.push(location);
    // console.log(this.path);
    // console.log(location);
    const Obj = {
      'frame': 3,
      'command': 16,
      'is_current': false,
      'autocontinue': true,
      'param1': 0,
      'param2': 0,
      'param3': 0,
      'param4': 0,
      'x_lat': location.lat(),
      'y_long': location.lng(),
      'z_alt': 5
    };

    this.wayPoints.push(Obj);
    console.log(this.wayPoints);

    this.flightPath = new google.maps.Polyline({
      path: this.path,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 4,
      map: this.map
    });

    this.flightPath.setMap(this.map);


    const marker = new google.maps.Marker({
      position: location,
      map: this.map,
      title: '#' + this.path.length,
      draggable: true,
    });

    const index = this.path.length - 1;

    this.markers[index] = { index: index, marker: marker };

    console.log(this.markers);

    /*
    marker.addListener('drag', (event) => {
      this.handleEvent(event);
    });
    */

    this.markers[index].marker.addListener('click', (event) => {
      console.log(index);
    });

    marker.addListener('dragend', (event) => {
      console.log(index);
      this.path[index] = event.latLng;
      // this.flightPath.setMap(this.map);
      this.flightPath.setPath(this.path);
    });

  }


  public setToLocation() {
    // this.places.bindTo('bounds', this.map);
    this.places.addListener('place_changed', function () {
      this.place = this.places.getPlace();
      console.log(this.place);
    });
  }


  // creating polygon

  public createPolygon() {
    // Construct the polygon.
    this.polygon = new google.maps.Polygon({
      paths: this.path,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35
    });
    this.polygon.setMap(this.map);
    this.addListenerOnPolygon(this.polygon);
  }

  // event to listen activities in polygon area
  public addListenerOnPolygon = function (polygon) {
    google.maps.event.addListener(polygon, 'click', function (event) {
      // this.toastr.success(`Can't create marker`);
      console.log(`Cannot set marker in polygon`);
    });
  };




  public setWayPoint: any = () => {
    this.appService.setWayPoint().subscribe((resp) => {
      console.log(resp);
      this.toastr.success(`Waypoints set successfully`);
    });
  }

  public takeOff_drone: any = () => {
    this.appService.takeOff().subscribe((resp) => {
      console.log(resp);
      this.toastr.info(resp.message);
    });
  }

  public land: any = () => {
    this.appService.land().subscribe((resp) => {
      console.log(resp);
      this.toastr.warning(resp.message);
    });
  }

  // setting dynamic waypoints OR set mission for drone
  public setWayPoints: any = () => {
    if (this.wayPoints) {
      this.appService.setWayPointDynamically(this.wayPoints).subscribe((resp) => {
        if (resp.success) {
          console.log(resp);
          this.toastr.success(`Waypoints set successfully`);
        } else {
          this.toastr.error(`Error while setting waypoints`);
        }

      });
    } else {
      this.toastr.error(`Please select Waypoints first`);
    }
  }

  // getting waypoints OR getting mission
  public getWaypoints: any = () => {
    this.appService.getMission().subscribe((resp) => {
      console.log(resp);
      if (resp.success) {
        for (const waypoint of resp.waypoints) {
          const loc = new google.maps.LatLng(waypoint.x_lat, waypoint.y_long);
          // this.map.panTo(loc);
          this.waypointmarker = new google.maps.Marker({
            position: loc,
            map: this.map,
            title: '#' + this.path.length,
            draggable: true,
          });

        }
        this.toastr.success(`${resp.wp_received} waypoints received`);
        console.log(resp.waypoints);
      } else {
        console.log(`No Waypoint selected`);
        this.toastr.error(`No waypoint selected`);
      }
    });
  }

  // clear waypoints OR clear mission
  public clearWaypoints: any = () => {
    this.appService.clearMission().subscribe((resp) => {
      console.log(resp);
      if (resp.success) {
        this.toastr.success(`Waypoints cleared`);
        // tslint:disable-next-line:forin
        for (const index in this.markers) {
          this.markers[index].marker.setMap(null);
        }
        this.flightPath.setMap(null);
        this.polygon.setMap(null);
        this.getWaypoints();
        console.log(this.markers);
      } else {
        this.toastr.error('Error occured, Please check');
      }

    });
  }

  // execute waypoints OR start mission
  public executeWaypoints: any = () => {
    this.appService.executeMission().subscribe((resp) => {
      console.log(resp.false);
      if (resp.success) {
        this.toastr.success(`Waypoints Execution Initiating`);
      } else {
        this.toastr.error(`Error occured while pending action like paused or Waypoint selection`);
      }
    });
  }


  // pause waypoints movements OR pause mission
  public pauseWaypoints: any = () => {
    this.appService.pauseMission().subscribe((resp) => {
      console.log(resp);
      if (resp.success) {
        this.toastr.success(`Waypoints movement paused`);
      } else {
        this.toastr.error(`Error occured, Please check actions`);
      }
    });
  }
}
